# What is this work for?

It's a final work presented as partial requirement to receaving the Bachelor Degree at Instituto Federal do Espírito Santo.

# What does it study?

The use of telemetry applied to electric power metering systems has made it possible to analyze the phasor behavior of consumers. This analysis not only allows the detection of irregular behavior such as fraud and metering failures, but also helps to identify unexpected effects on the power supply. The register of reverse active power flow in consumers without any installed generating capacity is one such effect. When a facility presents negative active power without having any mini or micro-generation contract drawn up, it is commonly believed that some irregularity is going on. However, after the analysis of some installations with this phasor behavior, it was observed that negative active power can result from authentic passive three-phase loads, causing undue displacements and commitment of resources for the inspection of false positive occurrences of irregularity. This paper demonstrates by means of mathematical modeling of the electric power supply that, under certain circumstances, passive three-phase loads can cause reverse active power flow in one or two of its phases. The circumstances under which such behavior can be observed are described and it is demonstrated that the total active power of a passive facility must always be positive.

# About sharing your own work.

If this work has somehow been useful to you, you should think about sharing your own work in a way it must be also useful to someone else.

# Related Repository

## [Fidelia](https://gitlab.com/daniel_pompermayer/fidelia)

Python package developed in the scope of this work for simulating electrical loads and for computation and plot of the complex power of the phases of those loads under normal or under fraudulent operations.

## Language

Portuguese

## External Resources

[Article published in 2018 13th IEEE International Conference on Industry Applications (INDUSCON)](https://ieeexplore.ieee.org/document/8627247)
# IFTEX - Modelo de TCC do Ifes
Template de Trabalho de Conclusão de Curso de acordo com as [Normas para Apresentação de Trabalhos Acadêmicos e Científicos de 2017](https://www.ci.ifes.edu.br/images/stories/2017/biblioteca/caderno_normas_tcc_2017-277_rev_27-11.pdf) do Instituto Federal de Educação, Ciência e Tecnologia do Espírito Santo.

## Autor

* **Humberto da Silva Neto**

## Licença

Este projeto está licenciado sob a licença LPPL - consulte o arquivo [LICENSE](LICENSE) para detalhes.
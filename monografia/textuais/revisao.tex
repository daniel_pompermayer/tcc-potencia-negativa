\chapter{REVISÃO BIBLIOGRÁFICA}
\label{cap:revisao_bibliografica}

A revisão bibliográfica deste trabalho consistiu da leitura de artigos revisados por pares constantes das bases bibliográficas Science Direct e IEEE e no repositório de trabalhos aprovados no Seminário Nacional de Distribuição de Energia Elétrica (SENDI).

\section{As perdas não técnicas no cenário internacional}

As perdas totais de energia elétrica são um dos indicadores monitorados e disponibilizados pelo banco de dados do Banco Mundial \citep{theworldbank}. Os dados de perdas dos mais diversos países são disponibilizados em séries temporais anuais, sendo os dados mais recentes datados de 2014. A tabela \ref{tab:perdas} apresenta os percentuais de perdas de energia elétrica de 20 países espalhados pelos 5 continentes nos anos de 2012, 2013 e 2014.

\begin{table}[htpb]
  \begin{center}
    \caption{Dados de Perdas de Energia Elétrica em Alguns Países nos Anos de 2012, 2013 e 2014.}
    \label{tab:perdas}
    \begin{tabular}{llll}
      \hline
      \multicolumn{1}{l|}{\textbf{País}} & \multicolumn{1}{l|}{\textbf{2012}} & \multicolumn{1}{l|}{\textbf{2013}} & \textbf{2014} \\
      \hline
      Angola             & 11.3\% & 11.3\% & 11.3 \%\\
      Argentina          & 13.6\% & 16.0\% & 14.3 \%\\
      Austrália          & 5.8 \% & 5.3 \% & 4.8  \%\\
      Brasil             & 17.1\% & 16.6\% & 15.8 \%\\
      Canadá             & 8.5 \% & 8.5 \% & 8.9  \%\\
      China              & 5.8 \% & 5.8 \% & 5.5  \%\\
      Egito              & 11.1\% & 11.1\% & 11.2 \%\\
      França             & 6.7 \% & 6.6 \% & 6.4  \%\\
      Alemanha           & 3.9 \% & 3.9 \% & 3.9  \%\\
      Índia              & 18.9\% & 18.5\% & 19.4 \%\\
      Itália             & 7.1 \% & 7.4 \% & 7.0  \%\\
      Japão              & 4.1 \% & 4.5 \% & 4.4  \%\\
      México             & 14.3\% & 14.3\% & 13.7 \%\\
      Nova Zelândia      & 6.8 \% & 6.7 \% & 6.5  \%\\
      Rússia             & 10.0\% & 10.1\% & 10.0 \%\\
      Senegal            & 16.3\% & 16.7\% & 12.8 \%\\
      África do Sul      & 8.7 \% & 8.5 \% & 8.4  \%\\
      Reino Unido        & 7.9 \% & 7.5 \% & 8.3  \%\\
      Estados Unidos da América & 6.3 \% & 6.0 \% & 5.9  \%\\
      Uruguai            & 12.2\% & 11.0\% & 9.6  \%\\
      \hline
    \end{tabular}
  \end{center}
  \source[\citet{theworldbank}]
\end{table}

Apesar da disponibilidade dos dados de perdas do Banco Mundial, \citet{Campi2018} argumentam que tecer comparações entre as perdas estimadas nos diversos países não é uma tarefa corriqueira. As legislações nesses países não são uniformes quanto aos métodos de estimativa e mesmo o que deve ou não ser considerado como perda de energia elétrica pode variar de um país para o outro. Os autores propõem uma metodologia própria capaz de estimar as perdas totais sob as perspectivas da geração e do consumo e, em seguida, de avaliar seus impactos econômicos. O método consiste de modelar todo o sistema elétrico em estudo como uma única barra onde estão ligadas todas as unidades geradoras e todas as unidades consumidoras. As particularidades de cada unidade são levadas em conta por meio do emprego da classificação do consumo e da geração utilizando a técnica de agrupamento de padrões similares (\textit{clustering similar patterns}). A metologia proposta foi testada com dados oriundos da Espanha.

\citet{Madrigal2017} afirmam que, de forma geral, a metodologia mais aplicada para a estimativa das perdas não técnicas é o cálculo da diferença entre as perdas totais e as perdas técnicas estimadas. Entretanto, os autores observam que a determinação precisa das perdas totais em um sistema de distribuição é desafiadora, uma vez que o número de circuitos a serem considerados nos cálculos é muito grande. Além disso, as informações normalmente disponíveis às distribuidoras são aquelas oriundas do processo de faturamento, o que significa que apenas em raras oportunidades tem-se acesso ao comportamento horário da demanda do sistema. Os autores propõem, então, um método para a estimativa direta das perdas de energia causadas por ligações clandestinas e fraudes. O método consiste da medição de variáveis estatisticamente relevantes em amostras representativas aleatoriamente escolhidas. São exemplos das variáveis aferidas: número de instalações clandestinas por setor, número de fraudes por setor, tipo de rede, número de medidores lacrados, número de medidores violados, etc. Ao fim, modelos matemáticos diversos são gerados por meio de regressão linear, aplicado pesos distintos às várias variáveis. A metodologia proposta foi testada em uma área sob o atendimento de uma distribuidora mexicana na qual 1476564 clientes eram servidos. Um dos modelos propostos alcançou nível de confiança de 99\%.

\citet{Messinis2018} produziram uma grande revisão de literatura em busca dos métodos de combate às perdas não técnicas empregados pelas distribuidoras de energia elétrica. Os autores afirmam que não se pode detectar uma única metodologia ou técnica aplicadas no combate às fraudes, mas que métodos dos mais diversos campos do conhecimento são empregados para esse fim. Os mais empregados seriam métodos de aprendizado de máquina, supervisão de anomalias e de diagnóstico da própria rede de distribuição. Os autores propõem a classificação dos métodos em três grandes categorias: métodos orientados a dados, métodos orientados a redes e métodos híbridos. Os métodos orientados a rede são aqueles que lançam mão da análise da rede e de sua topologia. São exemplos a medição descentralizada e o estudo do fluxo de potência na rede. Os métodos orientados a dados são aqueles que se utilizam exclusivamente dos dados de consumo do cliente. Por fim, os métodos híbridos seriam aqueles que usam ambas as abordagens. Nos métodos orientados a dados, uma série de parâmetros podem ser supervisionados, como índices estatísticos do consumo, fator de potência, fator de carga, coeficientes \textit{Wavelet} e coeficientes de Fourrier. Os métodos empregados para a análise orientada a dados mais comuns empregam algorítimos de inteligência artificial como Máquinas de Vetores de Suporte (SVM), Redes Neurais Artificiais (ANN), Árvores de Caminhos (OPF) e Vizinhos mais Próximos (k-nn) para aferir a legitimidade dos dados analisados. Em geral, classificadores supervisionados são mais empregados, embora a necessidade de um conjunto de dados catalogados para treino possa dificultar a sua implementação. Em contrapartida, classificadores não supervisionados são empregados em redes com casos raros de irregularidade ou para a detecção de novas fraudes sobre as quais não se possui informação capaz de gerar um bom conjunto de treinamento. Para os métodos orientados a redes, as técnicas mais empregadas são o monitoramento do fluxo de potência da rede, a estimativa do estado da rede por meio de dados de medição inteligente e o uso de sensores espalhados. Destaca-se porém a ausência de menção ao sinal da potência ativa aferida como parâmetro para detecção de irregularidades.

\citet{Viegas2017} também elaboraram uma extensa revisão bibliográfica na qual as diversas técnicas de combate às perdas não técnicas são visitadas. Os autores apontam os pontos do sistema de distribuição vulneráveis às fraudes segundo apontam os diversos artigos e explicitam quais tipos de irregularidade são mais comumente encontrados em cada zona do sistema. Os autores também classificam as técnicas apontadas em soluções de hardware, soluções sem hardware e estudos teóricos. Também nesse estudo não se faz menção a técnicas que empreguem o estudo do fluxo de potência ativa das unidades consumidoras.

Uma série de artigos recentes abordam diversas técnicas para o combate às perdas não técnicas. \citet{Ahmad2017} propõe o uso de medidores inteligentes para prevenir perdas não técnicas no Paquistão. \citet{Bula2016} também lançam mão de medidores inteligentes em sua técnica, propondo, porém, uma análise focada na análise da queda de tensão ao longo das longo  das linhas para a detecção de consumo irregular. \citet{Viegas2018} utilizam técnicas de agrupamento para classificar as unidades consumidoras segundo seu padrão de consumo. Os grupos criados passam a fornecer um comportamento médio padrão com o qual os comportamentos individuais de cada unidade são comparados. Unidades consumidoras que apresentam comportamento muito distinto do comportamento médio de seu grupo são enviadas para o processo de análise de irregularidades.

\section{As perdas não técnicas no cenário nacional}

No Brasil, o Submódulo 2.6 dos Procedimentos de Regulação Tarifária (PRORET) da ANEEL define as perdas técnicas regulatórias como a “parcela das perdas na distribuição inerente ao processo de transporte, de transformação de tensão e de medição da energia na rede da concessionária” \citep[p. 4]{PRORET2_6}. As perdas não técnicas, por sua vez, são definidas como sendo “todas as demais perdas associadas à distribuição de energia elétrica, tais como furtos de energia, erros de medição, erros no processo de faturamento, unidades consumidoras sem equipamento de medição, etc.” \citep[p. 4]{PRORET2_6}.

A figura \ref{fig:perdas} apresenta um quadro ilustrativo em que as perdas técnicas e não técnicas são ilustradas ao logo de toda a cadeia de fornecimento de energia elétrica. Percebe-se no último nó do sistema que as perdas não técnica são obtidas precisamente pela diferença entre as perdas totais aferidas e as perdas técnicas estimada.

\begin{figure}[htbp]
  \centering
  \caption{Perdas no Setor Elétrico}
  \centerline{\includegraphics[scale=0.75]{./Imagens/Perdas.jpg}}
  \source[\citet{aneel2018}]
  \label{fig:perdas}
\end{figure}

O módulo 2.6 do PRORET também institui a figura das perdas não técnicas regulatórias. Esse índice é um percentual definido nas revisões tarifárias que visam sopesar os custos das perdas não técnicas sobre as distribuidoras. Isso se dá devido ao reconhecimento por parte do órgão regulador de que, embora as empresas possuam gerência sobre uma parcela das perdas, uma série de fatores não gerenciáveis são afetados por características socioeconômicas das áreas sob concessão (e.g. níveis de urbanização renda, precariedade, grau de violência) \citet{lima2018}. As perdas não técnicas regulatórias são consideradas no cálculo da tarifa de energia elétrica e têm o seu custo repassado ao consumidor. Apresentando relatório em Audiência Pública na Câmara de Deputados do Brasil, em 2018, o Superintendente de Gestão Tarifária da ANEEL afirmou que as perdas não técnicas regulatórias representam cerca de 2,9\% das tarifas de distribuição \citep{lima2018}.

O cálculo que define o percentual regulatório das perdas não técnicas segue uma metodologia de regulação por incentivos. No cálculo são considerados comparativamente fatores como o desempenho das distribuidoras, critérios de eficiência e características socioeconômicas das áreas sob a concessão das empresas. Empresas com melhor desempenho tendem a reduzir o percentual regulatório para todas as demais. O custo das perdas que excedem o percentual regulatório onera diretamente o caixa das distribuidoras.

Dos dados públicos disponíveis no sítio eletrônico da \citet{aneel2018}, verifica-se que nos dez anos decorridos entre os anos de 2008 e 2017, a perda não técnica total alcançou patamares de 302 GWh, dos quais 31,5 GWh foram perdidos apenas no ano de 2017. A figura \ref{fig:perdas_graf} apresenta os valores percentuais de perdas técnicas, não técnicas e totais em cada ano desde 2008 a 2017.

\begin{figure}[htpb]
  \centering
  \caption{Perdas Percentuais no Setor Elétrico por Ano}
  \centerline{\includegraphics[width=16cm]{./Imagens/perdas_graf.png}}
  \source[Elaborado pelo autor com dados de \citet{aneel2018}]
  \label{fig:perdas_graf}
\end{figure}

A figura \ref{fig:perdas_dist} apresenta os volumes de perdas não técnicas percentuais por distribuidora nos anos de 2015, 2016 e 2017.

\begin{figure}[htpb]
  \centering
  \caption{Perdas Não Técnicas Percentuais por Distribuidora}
  \centerline{\includegraphics[width=16cm]{./Imagens/perdas_dist.png}}
  \source[Elaborado pelo autor com dados de \citet{aneel2018}]
  \label{fig:perdas_dist}
\end{figure}

% Ao se analisar a figura \ref{fig:perdas_dist}, torna-se visível uma discrepância gritante no desempenho das diversas distribuidoras no que tange ao combate às perdas. Essa discrepância se deve certamente - dentre outros fatores dos quais destaca-se a complexidade social das áreas atendidas - também à eficiência das distribuidoras no combate ao furto, à falha e à inadimplência.

A figura \ref{fig:perdas_regxafe} apresenta as perdas não técnicas percentuais aferidas pelas distribuidoras no ano de 2017 em contraposição às perdas não técnicas regulatórias.

\begin{figure}[ht]
  \centering
  \caption{Perdas Não Técnicas Percentuais x Perdas Não Técnicas Regulatórias Percentuais por Distribuidora em 2017}
  \centerline{\includegraphics[width=16cm]{./Imagens/perdas_regxafe_dist.png}}
  \source[Elaborado pelo Autor com dados de \citet{aneel2018}]
  \label{fig:perdas_regxafe}
\end{figure}

\FloatBarrier

Na figura, nota-se que, embora quase todas as distribuidoras tenham apresentado perdas superiores ao percentual regulatório no ano de 2017, as metas não se distanciam das perdas aferidas. Isso significa que grande parte do montante de energia desviado foi tarifado do consumidor.

% Nesse sentido, uma maior assertividade nas inspeções técnicas de combate a fraude é de evidente valia para as distribuidoras de energia elétrica. Porém, embora o Estado Brasileiro também seja \textit{stakeholder} de distribuidoras de energia elétrica em todo o país, o interesse público na melhoria dos processos de inspeção de irregularidade não se restringe ao eventual incremento dos dividendos das distribuidoras estatais. É também do interesse público, o decréscimo da tarifa na parcela referente aos furtos de energia elétrica e às falhas de medição.

Embora uma parcela considerável das perdas não técnicas seja reconhecida pelo regulador e incorporada à tarifa em cada ciclo tarifário, o excedente de perdas ainda onera o caixa da grande parte das distribuidoras do país, incluindo as concecionárias que atendem ao estado do Espírito Santo. Por essa razão, as empresas tem destinado equipes inteiras ao combate às fraudes e falhas de medição e investido grandes somas de dinheiro em programas de P\&D que propiciem resultados nessa direção. Além disso, mesmo as distribuidoras com melhor desempenho precisam evoluir constantemente a sua técnica de combate às fraudes pois, além dos fraudadores persistirem em constante inovação, cometendo fraudes cada vez mais complexas e criativas, a metodologia de reconhecimento do percentual regulatório tende a reduzir constantemente o total reconhecido. Apensa-se a isso ainda o projeto de lei nº 5.457 de 2016, em tramitação na Câmara dos Deputados do Brasil, que dispõe sobre a exclusão da base de cálculo das contas de energia elétrica da cobrança pela revisão de ligações clandestinas e inadimplência, e limita em 5\% as compensações por perdas técnicas e não técnicas na transmissão e distribuição de energia elétrica.

Uma iniciativa notável em direção ao combate ao furto de energia foi apresentada por \citet{baptista2016}. Os autores propõem uma topologia de rede de distribuição, batizada de BT ZERO, na qual não existe propriamente uma rede de baixa tensão, apenas ramais de energia já medida que saem diretamente dos postos de controle e medição às unidades consumidoras. A figura \ref{fig:btzero} apresenta uma foto da rede proposta.

\begin{figure}[htbp]
  \centering
  \caption{Topologia de Rede BT ZERO}
  \centerline{\includegraphics[height=10cm]{./Imagens/btzero.png}}
  \source[\citet{baptista2016}]
  \label{fig:btzero}
\end{figure}

Na figura nota-se a ausência da rede em baixa tensão bem como a localização dos medidores longe do alcance dos clientes. Os ramais que alimentam as unidades consumidoras são compostos de cabos concêntricos isolados, ou seja, os condutores fase são envoltos pelos condutores neutros de forma que uma ligação clandestina não possa ser realizada sem o rompimento do cabo. Na rede BT Zero, empregam-se transformadores de menor potência e que possuem uma blindagem especial nos pontos de conexão dos cabos. Além disso a topologia emprega medidores capazes de realizar corte e religação à distância, agilizando as medidas de combate à inadimplência.

\citet{macieira2018} propõem uma nova topologia de rede baseada na rede BT ZERO, mas adequada as zonas rurais do estado do Espírito Santo. Os autores apontam que, uma vez que em zonas rurais a concentração de unidades consumidoras é muito pequena, a utilização de topologias como a rede BT ZERO original mostram-se inviáveis. Dentre outras razões, uma das dificuldades encontradas foi a necessidade de projetar-se uma blindagem para os inúmeros modelos de transformadores utilizados nas zonas rurais. A solução proposta foi a substituição da blindagem empregada no projeto original por uma blindagem composta de uma manta de borracha e uma trama de fibra, podendo ser moldada no próprio transformador. Além disso, os concentradores de medição da nova topologia não empregam mais do que dois medidores. O estudo ainda apresenta alterações próprias ao cenário rural como a implementação de controle de partida de motores a fim de evitar a queima do equipamento de medição na partida de bombas e o uso de modais de telecomunicação que oferaçam cobertura mesmo em áreas remotas.

No cenário capixaba, outras soluções vêm sendo implementadas. \citet{roque2018} propõem um sistema capaz de identificar ligações clandestinas na iluminação pública e interromper o fornecimento de energia ao poste atacado. \citet{erick2018}, por sua vez, desenvolveram um dispositivo capaz de bloquear o eletroduto de clientes com o histórico de inadimplência e de se autoreligarem após cortes no fornecimento de sua energia elétrica.

No cenário nacional, muitos trabalhos recentes também se destacam na tentativa de combater as perdas não técnicas. \citet{leticia2018} se valem da ampla inserção de sistemas de medição e automatização nas redes de uma concecionária paulisa para regionalizar os índices de perdas e melhorar a efetivadade das ações de combate. \citet{filho2018}, por sua vez, utilizam os conceitos de Descoberta de Conhecimento em Bancos de Dados e de Mineração de Dados para combater o furto de energia elétrica. No trabalho de \citet{souza2018}, um sensor inteligente foi desenvolvido a fim de monitorar a rede em pontos estratégicos. Os sensores são interligados por uma unidade terminal remota empregando rádio e são espalhados pela rede de maneira a proporcionar uma avaliação do estado da rede de distribuição e do fluir da potência através de si. Os dados são apresentados em um software georeferenciado auxiliando os operadores do sistema a identificar de forma visível fluxos indevidos de potência.

\FloatBarrier

\section{A potência ativa negativa}
\label{sec:pot_ativa_negativa}

Dentre os artigos encontrados na pesquisa bibliográfica empreendida neste trabalho, dois foram indentificados como relacionados ao fluxo reverso de potência ativa.

Em um dos artigos, \citet{Klucznik2017} apontam que a presença de admitâncias shunt desequilibradas ao longo de uma linha de transmissão aérea a dois condutores pode fazer com que a potência ativa flua de um condutor ao outro. Esse fenômeno faz com que os medidores instalados em um dos condutores registrem potência ativa negativa naquele condutor. Essa ocorrência particular, porém, não se aplica aos casos de potência ativa negativa encontrados em unidades consumidoras ligadas ao sistema de distribuição, já que a topologia das redes de distribuição é muito diferente da topologia das redes de distribuição.

Por outro lado, o artigo de \citet{Kanalik2014} tem por objetivo justamente demonstrar que cargas assimétricas e variáveis podem causar potência ativa negativa em algumas de suas fases. O trabalho explora o fluxo reverso de potência ativa oriundo de fornos elétricos a arco (FEA), apresentando o equipamento como exemplo de uma carga real em que esse comportamento pode ser observado.

\citet{Kanalik2014} apontam duas causas para o surgimento do fluxo reverso de potência ativa no fornecimento de energia elétrica: mudanças repentinas na impedância da carga e desbalanceamentos severos.

Os efeitos da mudança repentina de impedância sobre corrente e potência de uma carga monofásica podem ser vistos na figura \ref{fig:mudanca_impedancia}.

\begin{figure}[ht]
  \centering
  \caption{Efeitos da Mudança Repentina de Impedância Sobre a Corrente e a Potência de uma Carga}
  \centerline{\includegraphics[width=16cm]{./Imagens/mudanca_impedancia.png}}
  \source[\citet{Kanalik2014}]
  \label{fig:mudanca_impedancia}
\end{figure}

Percebe-se na figura \ref{fig:mudanca_impedancia} que - durante o ciclo apresentado - a potência instantânea é negativa com mais intensidade e por mais tempo do que positiva. Uma vez que a potência ativa é matematicamente expressa pela média temporal da potência instantânea em um dado intervalo de tempo, a potência ativa mostra-se negativa no ciclo apresentado.

Os autores também demonstram que cargas desbalanceadas são razão para fluxo reverso de potência ativa, fornecendo um exemplo teórico de carga trifásica desbalanceada.

Após a análise teórica, \citet{Kanalik2014} apresentaram os dados de potência ativa adquiridos durante a operação de um FEA alimentado em 22 kV. A figura \ref{fig:pot_fea} apresenta o consumo de potência ativa do FEA nos primeiros ciclos após a sua ligação. Os dados foram amostrados a cada ciclo da tensão e são apresentados na figura tendo o índice dos ciclos sobre o eixo das ordenadas.

\begin{figure}[ht]
  \centering
  \caption{Consumo de Potência Ativa nas Fases de um FEA nos Primeiros Ciclos}  
  \centerline{\includegraphics[width=16cm]{./Imagens/pot_fea.png}}
  \source[\citet{Kanalik2014}]
  \label{fig:pot_fea}
\end{figure}

Percebe-se na figura \ref{fig:pot_fea} que a potência ativa da fase A (L1) é majoritariamente negativa, ficando explícito que cargas reais podem apresentar potência negativa de maneira autêntica.
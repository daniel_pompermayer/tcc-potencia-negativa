\chapter{INTRODUÇÃO}
\label{intro}

A energia elétrica é um insumo fundamental para as sociedades humanas tal qual elas se estabeleceram no pós revolução industrial. Produzida a partir dos mais diversos modos de conversão e desde fontes primárias como a energia hidráulica das quedas d'água ou a energia cinética dos ventos, a energia elétrica é comumente obtida em plantas de geração localizadas em regiões remotas e distantes dos grandes centros urbanos.

No Brasil, uma série de entes são empregados para o transporte da energia elétrica desde a sua geração até o seu consumo. Neste trabalho, destacam-se as distribuidoras de energia elétrica. Distribuidoras de Energia Elétrica são os agentes do setor elétrico brasileiro responsáveis pela gestão e manutenção dos ativos que compõem os sistemas de distribuição: estruturas de equipamentos, postes e condutores responsáveis por levar luz e força aos consumidores finais.

A energia elétrica distribuída aos consumidores é adquirida pelas distribuidoras seguindo o rito regulatório imposto pela Agência Nacional de Energia Elétrica (ANEEL). Essa energia é entregue às unidades consumidoras mediante a fatura de seu consumo por meio de medidores de energia elétrica - popularmente conhecidos como relógios. A diferença entre o montante de energia elétrica adquirido e o montante de energia elétrica distribuído e faturado constitui-se em perda de energia e tem como uma de suas causas o consumo irregular causado por fraudes no sistema de medição ou devido à má instalação do equipamento (i.e. falhas de medição).

O avanço tecnológico permitiu a implementação de equipamentos de telemetria e medição mais sofisticados. Essas ferramentas são empregadas pelas distribuidoras com o objetivo de fiscalizar e supervisionar o consumo das unidades consumidoras atendidas por sua rede de distribuição, evitando perdas causadas por consumo irregular.

% A tensão e a corrente elétricas alternadas são grandezas físicas que podem ser representadas por meio de entes matemáticos conhecidos como fasores. Fasores são representações matemáticas de sinais senoidais invariantes no tempo oriundas da aplicação da equação \eqref{equacao_euler}, conhecida como Equação de Euler.

% \begin{equation}
% 	\label{equacao_euler}
%     \underbrace{A \cdot \cos(\omega t + \phi)}_{Sinal~Senoidal} = \Re \{ \underbrace{A \cdot e^{j \cdot \omega t + \phi}}_{Fasor}\}
% \end{equation}

As tensões nas linhas que atendem as unidades e as correntes que percorrem os ramais que as alimentam são continuamente lidas pelos medidores modernos. Esses dados são utilizados para o cálculo de uma série de outras grandezas físicas representativas do consumo aferido e enviados às distribuidoras por meio de redes de telecomunicações para que se possa verificar a legitimidade do consumo faturado. Uma dessas grandezas é a potência ativa. % \daniel[inline]{A potência ativa é a grandeza física que mensura a quantidade de trabalho útil que um ente físico realiza em um intervalo de tempo, utilizando a eletricidade como força motriz}{Precisa de referência?}.

\citet{Carvalho2012} afirma que não havendo presença de geração distribuída dispersa na rede de distribuição, os sistemas de distribuição de energia elétrica são projetados para operar um fluxo de potência ativa radial e unidirecional. Isso implica dizer que entre os alimentadores das subestações e as unidades consumidoras existe apenas um único caminho para a circulação de corrente. Nessa circunstância, a potência ativa deveria fluir das linhas de distribuição às unidades, de forma que se caracteriza em fluxo reverso de potência ativa todo fluxo de potência ativa que flua das unidades às linhas. O fluxo reverso é matematicamente expresso por um sinal negativo no valor da potência ativa, de onde advém a expressão "potência ativa negativa".

A potência ativa é supervisionada com vistas à detecção de uma irregularidade comum de ser encontrada em unidades consumidoras ligadas aos sistemas de distribuição de energia elétrica: o mal posicionamento dos condutores nos bornes de medidores.

Condutores mal posicionados levam o medidor a interpretar erroneamente o fluxo de potência ativa da unidade consumidora, entendendo que a instalação que ora consume esteja a gerar energia elétrica. A Figura \ref{fig:fraude} apresenta um medidor com condutores da fase C invertidos: o condutor de linha encontra-se posicionado no borne de carga, enquanto o condutor de carga encontra-se posicionado no borne de linha.

\begin{figure}[htbp]
  \centering
  \caption{Medidor com os Condutores da Fase C Invertidos}
  \centerline{\includegraphics[height=11cm]{./Imagens/fraude.png}}
  \source[\citet{pompermayer2018}]
  \label{fig:fraude}
\end{figure}

O mal posicionamento de condutores nos bornes dos medidores pode ser fruto de erro na instalação do equipamento ou de manipulação indevida por parte do consumidor. Em ambos os casos tal irregularidade provocará erro na medição e redução indevida na fatura, uma vez que a energia total aferida será menor do que a energia total realmente consumida.

Essa irregularidade deveria ser de fácil detecção: constatar-se-ia a presença de uma anomalia sempre que os medidores de uma unidade consumidora indicassem a geração de energia elétrica sem que houvesse contrato de micro ou minigeração distribuída lavrado. O mesmo poderia ser dito quando os medidores de micro e mini geradores distribuídos indicassem geração de energia superior ao acordado ou em horário distinto daquele que o modal de geração o permite (e.g. não é possível haver geração fotovoltaica durante a noite). Porém a aplicação deste pensamento tem produzido um elevado número de falsos positivos nas inspeções de irregularidade oriundas destes apontamentos.

Verificou-se que algumas unidades consumidoras ligadas à rede de distribuição, embora não possuam nenhuma capacidade de geração instalada, apresentam fluxo reverso de potência ativa em uma ou duas de suas fases. Em inspeções de irregularidade, constatou-se que em uma considerável parcela destas unidades, o fluxo reverso de potência não fora ocasionado por fraude ou falha de medição, mas por uma característica legítima de carga. Verificou-se ainda que, em não raras oportunidades, o comportamento inusitado relaciona-se com a presença de bancos de capacitores avariados.

\section{Objetivos}

O presente estudo tem por objetivo melhor compreender a ocorrência do fluxo reverso de potência ativa causado por características legítimas de cargas. Almeja-se buscar soluções para a caracterização do fenômeno que ajudem a evitar o empenho de recursos e esforços na análise e inspeção de falsos positivos. Estuda-se a hipótese de que cargas trifásicas genéricas, lineares e passivas, em certas circunstâncias, sejam legitimamente capazes de ocasionar fluxo de potência ativa reverso em alguma de suas fases.

Os objetivos específicos que nortearam a pesquisa foram:

\begin{itemize}
\item Modelar matematicamente o fornecimento de energia elétrica a unidades consumidoras ligadas a um sistema de distribuição de energia elétrica;

\item Modelar matematicamente as fraudes de inversão de condutores de corrente nos bornes do medidor e de inversão da sequência de fases das tensões com relação à sequência de fases das correntes;

\item Aplicar as topologias de cargas trifásicas delta e estrela com neutro aterrado aos modelos obtidos;

\item Aplicar desbalanceamentos severos ao modelo de cargas trifásicas conectadas em delta;

\item Desenvolver uma ferramenta em linguagem Python capaz de:

  \begin{itemize}
  \item Simular o consumo de potência de um amplo espectro de cargas trifásicas nas topologias delta e Estrela com Neutro Aterrado;
  \item Simular o comportamento de cargas fraudulentas; 
  \item Produzir Diagramas de Argang-Gauss das potências complexas das cargas simuladas;
  \end{itemize}

\item Empregar a ferramenta desenvolvida para simular o consumo de potência de um amplo espectro de cargas trifásicas nas topologias delta e estrela com neutro aterrado;

\item Empregar a ferramenta desenvolvida para simular o comportamento de cargas fraudulentas;

\item Ensaiar cargas RLC reais - desbalanceadas ou não - para confirmação dos apontamentos teóricos;

\end{itemize}

\section{Organização do trabalho}

As seções seguintes dividem-se entre Revisão Bibliográfica, Metodologia, Resultados e Discussões e Conclusão. A seção de Revisão Bibliográfica destina-se a posicionar o objeto em estudo no escopo da discussão acadêmica e a sustentar a sua pertinência por meio de trabalhos que demonstrem a necessidade do estudo empreendido. A seção de Metodologia apresenta em detalhes as metodologias científicas empregadas na elaboração deste trabalho, explicando os cálculos e processos, permitindo sua falseabilidade ou sua reprodução. Em Resultados e Discussões expõem-se os resultados alcançados pela aplicação da metodologia, tecendo discussões que os tornem adequados ao caso concreto. A Conclusão resume o estudo, apresentando em breves termos os apontamentos concretos alcançados.
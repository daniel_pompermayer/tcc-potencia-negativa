\chapter{METODOLOGIA}
\label{cap:metodologia}

Para que se aprofundasse o conhecimento acerca do fluxo de potência ativa no fornecimento de energia elétrica, o fornecimento de energia elétrica de unidades consumidoras de um sistema de distribuição foi modelado matematicamente. Dessa maneira, foram obtidas as equações que regem a potência consumida - ou fornecida - por cada instalação.

De posse das equações de potência, foram estudadas as circunstâncias nas quais a potência ativa pode ser negativa em ao menos uma das fases em cargas trifásicas do tipo estrela com neutro aterrado e delta.

O estudo puramente analítico das equações de potência mostrou-se suficiente e conclusivo para a topologia estrela com neutro aterrado. Por outro lado, estudar analiticamente as equações geradas pela topologia delta mostrou-se um exercício sofisticado de matemática. Decidiu-se empregar duas ferramentas adicionais que embora não possuam a mesma elegância matemática, são eficazes para a obtenção de resultados: a inserção de desequilíbrios de carga severos (i.e. interrupção da circulação de corrente em uma e em duas fases) e a análise numérica das equações por meio de um simulador programado em linguagem Python.

A metodologia exposta nessa seção foi parcialmente testada em \citet{pompermayer2018}, trabalho no qual expuseram-se alguns resultados parciais que são novamente apresentados neste trabalho. Desta vez, porém, esses resultados encontram-se amparados em teoria mais sólida e são destrinchados com mais profundidade.

\section{Modelagem matemática do fornecimento de energia elétrica}
\label{sec:modelagem}

A modelagem matemática do fornecimento de energia elétrica a unidades consumidoras ligadas a um sistema de distribuição seguiu a metodologia clássica da análise de circuitos na Engenharia Elétrica. Atribuiu-se a cada componente um símbolo gráfico dotado de um significado matemático conhecido, aplicaram-se as simplificações permitidas pelo escopo do problema e resolveu-se o circuito final por meio do emprego das equações básicas da eletricidade.

A figura \ref{fig:unifilar} apresenta o modelo inicialmente proposto.

\begin{figure}[htbp]
  \centering
  \caption{Diagrama Unifilar do Fornecimento de Energia Elétrica}
  \scalebox{0.7}{\input{./Imagens/unifilar.tikz}}
  \label{fig:unifilar}
  \source
\end{figure}

\subsection{Modelagem do sistema de distribuição de energia elétrica}

Dada a intenção de se estudar mais aprofundadamente a direção do fluxo de potência ativa de cada unidade consumidora - e não outros parâmetros mais sensíveis aos aspectos construtivos da rede -, escolheu-se desconsiderar as particularidades do sistema de distribuição, enxergando-o como uma barra de alimentação de capacidade infinita. Essa escolha impõe ao modelo a limitação de não considerar eventuais afundamentos de tensão causados por excesso de carga e quedas de tensão causadas pelas impedâncias internas dos equipamentos que compõem a rede de distribuição. Essas limitações, porém, não são relevantes no estudo do sentido do fluxo de potência. 

O sistema de distribuição, cujas tensões $\dot V_A$, $\dot V_B$, e $\dot V_C$ - que são reguladas por norma e não podem variar muito em amplitude e em fase - são todas equilibradas, é representado na figura \ref{fig:unifilar} por uma fonte senoidal regida pelo sistema de equações \eqref{eq:fonte}.

\begin{align}
	\label{eq:fonte}
    V_A &= A \cdot cos(120 \pi t)\\\nonumber
    V_B &= A \cdot cos(120 \pi t - \ang{120})\\\nonumber
    V_C &= A \cdot cos(120 \pi t + \ang{120})\\\nonumber
\end{align}

Em \eqref{eq:fonte}, A é o valor máximo de tensão da rede de distribuição no ponto em estudo.

\subsection{Sistema por unidade}
\label{sec:por_unidade}

Segundo \citet{Stevenson1994}, tensão, corrente, potência e admitância são grandezas elétricas comumente expressas por unidades de uma grandeza de base, isto é, unidades de um valor de referência especificado para cada grandeza. Dessa maneira, o valor de cada grandeza seria expresso por meio da razão entre seu valor original e o valor de base determinado. Essa forma de representação é conhecida como Sistema por Unidade (p.u.).

As equações de \eqref{eq:fonte} podem ser ainda mais simplificadas pela adoção de análise em p.u.

\citet{Stevenson1994} afirmam também que as grandezas de tensão, corrente, potência e admitância estão tão fortemente correlacionadas que a adoção de bases para duas delas é suficiente para a determinação dos valores de base de todas as demais. Ao fim e ao cabo, este trabalho objetiva ajudar a estabelecer parâmetros por meio dos quais uma distribuidora de energia pode aferir a autenticidade do consumo de clientes que apresentam fluxo negativo de potência ativa. Nesse sentido, é de extrema importância garantir a reprodutibilidade do experimento, o que perpassa pela escolha adequada dos parâmetros de base do sistema p.u.

Dois parâmetros de base para o sistema p.u., adequados sob a ótica da reprodutibilidade, são:

\begin{enumerate}
\item A tensão do sistema de distribuição de energia elétrica que atende ao cliente; e
\item A admitância de uma das fases da maior carga permitida a esse cliente.
\end{enumerate}

A maior carga permitida ao cliente é uma carga trifásica em delta cuja potência máxima demandada da rede é igual em módulo à potência máxima estabelecida pelo contrato de fornecimento de energia elétrica firmado entre a distribuidora e o cliente.

O sistema \eqref{eq:fonte}, em p.u. pode ser reescrito na forma de \eqref{eq:fonte_pu}.

\begin{align}
	\label{eq:fonte_pu}
    V_A &= 1 \cdot cos(120 \pi t)\\\nonumber
    V_B &= 1 \cdot cos(120 \pi t - \ang{120})\\\nonumber
    V_C &= 1 \cdot cos(120 \pi t + \ang{120})\\\nonumber
\end{align}

Aplicando a equação de Euler às equações de \eqref{eq:fonte_pu}, obtém-se as equações que modelam o sistema de distribuição em sua forma fasorial.

\begin{align}
	\label{eq:fonte_pu_fasorial}
    V_A &= 1\\\nonumber
    V_B &= \alpha^2\\\nonumber
    V_C &= \alpha\\\nonumber
\end{align}

Em \eqref{eq:fonte_pu_fasorial}, $\alpha = 1 \cdot e^{j \ang{120}}$ ou, como serão representados os números complexos em forma polar neste trabalho, $\alpha = 1  \angle{\ang{120}}$.

\subsection{Modelagem dos ramais de ligação}

Como normalmente os ramais de ligação que conectam os clientes à rede são demasiadamente curtos, os efeitos resistivos e reativos dos condutores foram desprezados. Essa simplificação impõe ao modelo a limitação de não se considerarem eventuais quedas de tensão ou elevações de tensão causadas pelos ramais, bem como consumo e/ou fornecimento de reativos por esses cabos. Dada a pequena extensão desses condutores, esses efeitos trariam apenas complexidade desnecessária aos cálculos, acrescentando muito pouca - ou nenhuma - precisão ao resultado. Os ramais foram representados na figura \ref{fig:unifilar} por uma barra conectando a fonte ao restante do circuito, sem influência alguma sobre o sinal que a percorre.

\subsection{Modelagem do transformador abaixador privado}

O modelo proposto também contempla a presença de um transformador abaixador privado. Transformadores abaixadores privados são equipamentos pertencentes a clientes atendidos em média tensão e necessários para a conexão de suas cargas à rede de distribuição. Transformadores são por si só sistemas complexos e carentes de modelagem própria. Por essa razão, diversos artigos podem ser encontrados nas bases bibliográficas relacionadas à Engenharia Elétrica em que a modelagem desse tipo de equipamento é tratada com maior profundidade. São os casos de \citet{Dzafic2015}, \citet{Rodas2007}, \citet{Gorman1992} entre outros.

\citet{Chen1991} procuram introduzir um modelo de transformador que auxilie na análise matemática de problemas de sistemas de distribuição desbalanceados de larga-escala tais como cálculo de fluxo de potência, cálculo de curto-circuito, perdas técnicas e estudos de proteção. Dentre os transformadores modelados, encontra-se o transformador trifásico com primário conectado em delta e secundário conectado em estrela com neutro aterrado. Esse é o transformador mais comumente empregado para acoplamento de cargas às redes de distribuição de energia elétrica em média tensão. % Segundo o trabalho, um transformador trifásico nessas circunstâncias pode ser modelado por duas barras interligadas por uma carga de matriz de admitâncias $\mathbf{Y}_T^{abc}$ e que alimentam uma carga \textit{shunt} representativa das perdas no núcleo do equipamento. A figura \ref{fig:modelo_trafo} apresenta o modelo proposto.

% \begin{figure}[ht]
%   \centering
%     \scalebox{0.7}{\input{./Imagens/modelo_trafo.tikz}}
%   \caption{Modelo Genérico de Transformador Proposto por \citet{Chen1991}}
%   \source{Adaptado de \citet{Chen1991}}
%   \label{fig:modelo_trafo}
% \end{figure}

% A matriz de admitâncias $\mathbf{Y}_T^{abc}$ é na verdade uma supermatriz composta de outras quatro matrizes de admitâncias que representam as cargas trifásicas que interligam as barras de primário e secundário.
% A equação \eqref{eq:supermatriz} apresenta a composição da matriz \citep{Chen1991}.
% 
% \begin{align}
%   \mathbf{Y}_T^{abc} =
%   \begin{tabular}{c|cc}
%     Barras & P         & S           \\ \hline
%     P      & $\mathbf{Y}_{II}$  & $\mathbf{Y}^t_{III}$ \\
%     S      & $\mathbf{Y}_{III}$  & $\mathbf{Y}_I$       \\
%   \end{tabular}
%   \label{eq:supermatriz}
% \end{align}
% 
% As matrizes $\textbf{Y}_{I}$, $\textbf{Y}_{II}$ e $\textbf{Y}_{III}$ são dadas apresentadas respectivamente nas equações \eqref{eq:y_i}, \eqref{eq:y_ii} e \eqref{eq:y_iii} \citep{Chen1991}.
% 
% \begin{align}
%     \mathbf{Y}_{I} = 
%     \left [ \begin{matrix}
%     	y_t & 0 & 0 \\
%         0 & y_t & 0 \\
%         0 & 0 & y_t \\
%    	\end{matrix} \right ]
%     \label{eq:y_i}
% \end{align}
% 
% \begin{align}
%     \mathbf{Y}_{II} = \frac{1}{3}
%     \left [ \begin{matrix}
%     	2y_t & -y_t & -y_t \\
%         -y_t & 2y_t & -y_t \\
%         -y_t & -y_t & 2y_t  \\
%    	\end{matrix} \right ]
%     \label{eq:y_ii}
% \end{align}
% 
% \begin{align}
%     \mathbf{Y}_{III} = \frac{\sqrt{3}}{3}
%     \left [ \begin{matrix}
%     	-y_t & y_t  & 0   \\
%            0 & -y_t & y_t \\
%          y_t &  0   & -y_t\\
%    	\end{matrix} \right ]
%     \label{eq:y_iii}
% \end{align}
% 
% Nas equações \eqref{eq:y_i}, \eqref{eq:y_ii} e \eqref{eq:y_iii}, $y_t$ representa a admitância de dispersão .

O modelo proposto por \citet{Chen1991} é composto por uma carga delta alimentada pela barra do primário, por uma carga estrela com neutro aterrado alimentada pela barra do secundário e por uma carga série que interliga as barras do primário e do secundário. O modelo de transformador delta-estrela com neutro aterrado pode ser representado na forma da figura \ref{fig:modelo_trafo_dist}.

\begin{figure}[htpb]
  \centering
  \caption{Modelo de Transformador delta-estrela com neutro aterrado Proposto por \citet{Chen1991}}
  \scalebox{1}{\input{./Imagens/modelo_trafo_dist.tikz}}
  \source[Adaptado de \citet{Chen1991}]
  \label{fig:modelo_trafo_dist}
\end{figure}

Da análise da figura \ref{fig:modelo_trafo_dist} depreende-se que as cargas ligadas tanto à barra do primário quanto à barra do secundário podem ser incluídas na análise das condições de carga das quais trata as seções \textit{a posteriori}. Com essa simplificação, porém, o modelo torna-se insensível às particularidades do transformador no que tange às perdas no núcleo e àquelas perdas modeladas por essas cargas, mas ainda compatível com o objeto deste trabalho.

A carga série, por sua vez, representa a influência das admitâncias de dispersão e de acoplamento no modelo do transformador. Como transformadores de distribuição são contruídos com especial atenção à dispersão e ao acoplamento eletromagnético, com núcleos em material adequado e com entreferros minimizados, não há grande prejuízo para a análise em curso em se desconsiderar a influência desta carga. Com essa simplificação, o modelo torna-se insensível aos efeitos da dispersão e do acoplamento. Este estudo, porém, não é sensível a esses efeitos.

Diante de todas as simplificações impostas, o transformador privado foi considerado ideal. Um transformador ideal não exerce influência alguma sobre o circuito em uma análise em p.u.

\subsection{Modelagem dos transformadores de potencial e de corrente para medição}

Em alguns casos, os dados fasoriais adquiridos pelos medidores que aferem o consumo das unidades ligadas ao sistema de distribuição são obtidos por meio de transformadores de potencial (TP) e de corrente (TC). Esses instrumentos também foram contemplados no modelo. 

Porém, segundo \citet{OLIVEIRA2001}, feitas todas as considerações prescritas pela NBR 6856 de 1992 \citep{AssociacaoBrasileiradeNormasTecnicas1992}, os TCs empregados para medição possuem erro de módulo inferior a 2\% e erro de fase inferior a \ang{2}. A autora também pontua que os TPs de medição são projetados de maneira a limitar os erros de módulo a uma fração percentual abaixo das condições de variação de tensão às quais a carga pode estar submetida e a limitar os erros de fase a valores inferiores a \ang{0,1}. 

Dessa maneira, se for considerado o disposto nas legislações bem como os aspectos construtivos desses instrumentos, eles podem ser omitidos sem prejuízo à análise em curso.

\subsection{O modelo simplificado}

Feitas essas considerações, o fornecimento de energia elétrica a uma unidade consumidora ligada a uma rede de distribuição pode ser modelado pelo diagrama unifilar simplificado apresentado na figura \ref{fig:unifilar_simplificado}.

\begin{figure}[htbp]
  \centering
  \caption{Diagrama Unifilar Simplificado do Fornecimento de Energia Elétrica}
  \scalebox{0.7}{\input{./Imagens/unifilar_simplificado.tikz}}
  \source
  \label{fig:unifilar_simplificado}
\end{figure}

\section{As equações de potência complexa}

As potências complexas das fases de uma carga trifásica, tais como vistas pelo sistema de medição, são dadas pelo produto da tensão de cada fase pelo conjugado da corrente da mesma fase. As correntes, por sua vez, resultam do produto vetorial da matriz de admitâncias da carga pelas tensões sobre suas fases. As equações \eqref{eq:correntes} e \eqref{eq:potencias} sintetizam matematicamente a relação existente entre a matriz de admitâncias de uma carga, as tensões sobre ela, as correntes elétricas que fluem nas linhas para a sua alimentação e as potências complexas drenadas da rede.

\begin{equation}
  \label{eq:correntes}
  \left [ \begin{matrix}\dot{I}_A\\\dot{I}_B\\\dot{I}_C\\\end{matrix} \right ] = \mathbf{Y} \times \left [ \begin{matrix}\dot{V}_A\\\dot{V}_B\\\dot{V}_C\\\end{matrix} \right ]
  \end{equation}
  
  \begin{equation}
  \label{eq:potencias}
   \left [ \begin{matrix}S_A\\S_B\\S_C\\\end{matrix} \right ] = \left [ \begin{matrix}\dot{V}_A\\\dot{V}_B\\\dot{V}_C\\\end{matrix} \right ] \cdot \left [ \begin{matrix}\dot{I}^*_A\\\dot{I}^*_B\\\dot{I}^*_C\\\end{matrix} \right ]
   \end{equation}

A potência complexa total da carga trifásica pode ser obtida pelo somatório das potências complexas de cada fase. A potência ativa total, por sua vez, corresponde à parte real da potência complexa total. As equações \eqref{eq:pot_total} e \eqref{eq:pot_ativa_total} expressam matematicamente a relação entre as potências complexa total e ativa total.

\begin{align}
  \label{eq:pot_total}
  S = S_A + S_B + S_C
\end{align}

\begin{align}
  \label{eq:pot_ativa_total}
  P = \Re[S]
\end{align}

\section{Modelagem das irregularidades}
\label{sec:modelo_irregularidades}

Este trabalho concentra-se no estudo dos efeitos de dois tipos de irregularidades: a inversão dos condutores de corrente de uma das fases nos bornes de um medidor de energia elétrica trifásico e a inversão da sequência de fases das tensões em relação à sequência de fases das correntes também nos bornes do medidor.

O efeito da inversão dos condutores de corrente de uma das fases sobre a modelagem matemática expressa-se pelo aparecimento de um sinal negativo na linha correlata à fase afetada na matriz de admitâncias. Dessa forma, matematicamente, aplicar uma fraude de inversão de condutores de corrente significa multiplicar a linha da fase afetada da matriz de admitâncias da carga por -1.

De igual forma, o efeito da inversão da sequência de fases das tensões em relação à sequência de fases de corrente expressa-se matematicamente pela troca de posição das duas linhas da matriz de admitâncias da carga correlatas às fases cujos condutores de tensão foram invertidos.

\section{A inserção de desequilíbrio de cargas}
\label{sec:desequilibrio}

Um dos apontamentos oriundos dos falsos positivos de fluxo de potência ativa reverso indevido é a suspeita de que desequilíbrios severos (e.g. defeitos em uma ou duas das fases de cargas trifásicas) podem ocasionar potência ativa negativa em uma ou duas das fases da unidade consumidora.

Afim de se averiguar a validade desse apontamento, simulou-se a abertura (i.e. interrupção na circulação de corrente) de uma e de duas fases de cargas trifásicas. A simulação da abertura de carga se dá pela fixação do valor zero nas admitância das fases afetadas.

As equações resultantes produzem diagramas de Argand-Gauss em que o fluxo de potência ativa é visivelmente representado sobre um plano cartesiano. Nos diagramas produzidos, o eixo real comporta as potências ativas (P), enquanto o eixo imaginário comporta as potências reativas (Q). A figura \ref{fig:argang_gauss} apresenta um diagrama de Argang-Gauss representando a potência complexa $\text{S} = \text{P}+j\text{Q}$.

\begin{figure}[htbp]
  \centering
  \caption{Diagrama de Argand-Gauss da Potência S}
  \centerline{\scalebox{0.8}{\input{./Imagens/argand_gauss.tikz}}}
  \source
  \label{fig:argang_gauss}
\end{figure}

Uma vez que a potência ativa é a parte real da potência complexa, sempre que a potência complexa estiver localizada nos quadrantes à direita do eixo vertical (i.e. primeiro e quarto quadrantes), a potência ativa será positiva. Por outro lado, sempre que a potência complexa estiver localizada nos quadrantes à esquerda do eixo vertical (i.e. segundo e terceiro quadrantes), a potência ativa será negativa. A figura \ref{fig:sinal_pot_ativa} apresenta os sinais da potência ativa segundo a localização da potência complexa no Diagrama de Argand-Gauss.

\begin{figure}[htbp]
  \centering
  \caption{Sinal da Potência Ativa Segundo a Localização da Potência Complexa no Diagrama de Argand-Gauss}
  \centerline{\scalebox{1}{\input{./Imagens/quadrantes.tikz}}}
  \source
  \label{fig:sinal_pot_ativa}
\end{figure}

\section{A análise numérica}
\label{analise_numerica}

A análise trigonométrica das equações de potência complexa na topologia delta mostrou-se complexa. Escolheu-se um caminho mais simples, porém menos elegante: analisar numericamente o comportamento de cargas com essa topologia. Produziu-se, para isso, uma biblioteca em linguagem de programação Python empregando as bibliotecas NumPy, Itertools e Matplotlib.

\subsection{A biblioteca fidélia}

Em homenagem ao grande escritor brasileiro Machado de Assis e às suas personagens femininas marcantes, a biblioteca desenvolvida foi batizada de Fidélia.

Fidélia é personagem do último romance de Assis, \textit{Memorial de Aires}. A jovem viúva, portadora da ousadia de ser uma mulher abolicionista no Brasil imperial, é conhecida por sua pureza de caráter e por sua capacidade analítica. A personagem recebe de Aires, por menção às suas qualidades, a descrição: "O maior valor dela está, além da sensação viva e pura que lhe dão as cousas, na concepção e  na  análise  que  sabe  achar  nelas" ~\citep[p. 34]{assis1908}. O nome da biblioteca não deixa de ser uma provocação: deseja-se que Fidélia - com seu poder de análise - seja uma ferramenta para que se flagre a infidelidade no consumo de energia elétrica.

A biblioteca é uma evolução do módulo \textit{Load Variation}, disponível em \citet{pompermayer2018b}. O módulo \textit{Load Variation} foi empregado para a análise numérica de cargas trifásicas em delta em \citet{pompermayer2018}, mas foi descontinuado em função do advento de Fidélia. Em Fidélia os processos se tornaram mais robustos e automatizados.

\subsection{Fidélia é um software livre} 

O projeto encontra-se disponível livremente em \url{https://gitlab.com/daniel_pompermayer/fidelia/}, sendo possível redistribuí-lo e modificá-lo de acordo com os termos da Licença Pública Geral GNU Versão 3 (GPLv3) tal qual publicada pela Fundação Software Livre (FSF). É preciso referir-se sempre à versão 3 da Licença, ou a qualquer versão posterior.

A GPLv3 é uma iniciativa da FSF e possui como fundamento, quatro liberdades que todo usuário de programas de computador deveria ter \citep{smith2007}:

\begin{enumerate}
\item A liberdade de usar o programa para qualquer propósito;
\item A liberdade de adaptar o código às suas necessidades;
\item A liberdade de compartilhar o programa com amigos e vizinhos;
\item A liberdade de compartilhar as mudanças realizadas no programa.
\end{enumerate}

A GPLv3 assegura o livre direito de cópia do programa licenciado, bem como exige que todo programa derivado seja igualmente livre.

\subsection{O simulador}
\label{sec:simulador}

Fidélia possui, dentre outros, um módulo chamado \textit{load.py} que fornece as classes \textit{Load} e \textit{LinSet} e a função \textit{new\_matrix} e um módulo chamado \textit{fraud.py} que fornece as funções \textit{current\_inversion}, \textit{sequence\_inversion} e \textit{cheat}.

A classe \textit{Load} permite instanciar objetos que armazenam as matrizes de admitâncias de uma carga elétrica polifásica e suas tensões de fase. Uma vez fornecidas todas as informações necessárias, os \textit{Loads} calculam automaticamente a tensão de seu neutro (se houver um neutro), as correntes elétricas nas linhas que os alimentam e as potências complexas drenadas da rede.

Já a classe \textit{LinSet} permite instanciar objetos que armazenam conjuntos de cargas trifásicas linearmente distribuídas. Um \textit{LinSet} é gerado pela combinação exaustiva de um conjunto de admitâncias. Ao instanciar o objeto, o usuário deve dizer quantos módulos e quantos argumentos devem ser contemplados pelas admitâncias das cargas simuladas, além do espectro de cada um desses parâmetros. As inúmeras admitâncias geradas são combinadas exaustivamente para formar tantas cargas quanto possível segundo a topologia indicada pelo usuário na instanciação. O \textit{LinSet} exige também um vetor de tensões de fase que será utilizado para alimentar todas as cargas criadas.

A função \textit{new\_matrix} é utilizada internamente pelos \textit{LinSets} para a geração das matrizes de admitâncias das cargas que eles armazenam.

Valendo-se dos apontamentos feitos na seção \ref{sec:modelo_irregularidades}, a função \textit{current\_inversion} aplica um fator de -1 nas linhas de uma matriz de admitâncias nas fases afetadas por uma irregularidade de inversão de corrente. Já a função \textit{sequence\_inversion} inverte duas das linhas de uma matriz de admitâncias nas fases indicadas pelo usuário, simulando a inversão da sequência de fases das tensões com relação à sequência de fases das correntes.

A função \textit{cheat}, por sua vez, aplica uma das duas irregularidades suportadas em um novo \textit{Load} (ou \textit{LinSet}) criado a partir de um \textit{Load} (ou \textit{Linset}) passado por parâmetro. Pode-se dizer que \textit{cheat} simula a aplicação de uma irregularidade em uma dada carga.

Fidélia também possui um módulo chamado \textit{plotter.py} que fornece a classe \textit{Plotter} e outras classes derivadas. A classe \textit{Plotter} e suas filhas permitem que um conjunto de potências complexas seja impresso em um plano de Argang-Gauss, dividindo esse conjunto de potências em subconjuntos segundo a regra de cada classe. Foram programadas classes que dividem as potências segundo o quadrante que ocupam e segundo o sinal de sua parte real.

Empregando a biblioteca Fidélia, um módulo Python foi desenvolvido para que se analisasse numericamente o fornecimento de energia elétrica a cargas trifásicas em Estrela com Neutro Aterrado e em Delta. O código em Python do módulo desenvolvido encontra-se no apêndice \ref{app:simulador}

O módulo empregou a classe \textit{LinSet} para instanciar dois \textit{LinSets} diferentes: um para a simulação de cargas em estrela com neutro aterrado e outro para a simulação de cargas em delta. Os \textit{LinSets} empregaram 11 valores de módulo - variando de 0 p.u. a 1 p.u. - e 15 valores de ângulo - variando de \ang{-90} a \ang{90} - para produzir um espaço linear complexo com 151 admitâncias. Ao todo, os objetos armazenaram 3442951 cargas trifásicas distintas cada um, fruto da combinação das 151 admitâncias nas três fases de cada carga.

Os \textit{LinSets}, alimentados por uma fonte trifásica equilibrada modelada pela equação \eqref{eq:fonte_pu_fasorial}, automaticamente calcularam as correntes nas linhas que alimentam as cargas e as potências drenadas da rede por cada carga.

Em seguida, o módulo empregou a classe \textit{Plotter} para gerar diagramas de Argand-Gauss contendo as potências complexas de cada fase e as potências complexas totais de cada carga.

\section{O ensaio de cargas reais}
\label{sec:met_cargas_reais}

O ensaio com cargas reais foi realizado em se utilizando um banco de cargas RLC e um medidor de precisão Yokogawa WT330®. A figura \ref{fig:ensaio} apresenta uma foto dos equipamentos utilizados.

\begin{figure}[htpb]
  \centering
  \caption{Banco de Cargas (em verde ao fundo) e Medidor Utilizado no Ensaio.}
  \includegraphics[height=10cm]{./Imagens/ensaio.png}
  \source
  \label{fig:ensaio}
\end{figure}

O banco de cargas é formado por três conjuntos de dez cargas resistivas, três conjuntos de dez cargas indutivas e três conjuntos de dez cargas capacitivas. Cada carga possui uma chave Liga/Desliga permitindo sua associação em paralelo às cargas do conjunto ao qual pertence. No ensaio foi utilizada apenas uma carga de cada conjunto.

Ensaiaram-se catorze cargas trifásicas conectadas em delta com as mais diversas combinações de indutor, capacitor, resistor e circuito aberto. As combinações ensaiadas são apresentadas no quadro \ref{quad:combinacoes}.

\begin{quadro}[htpb]
  \caption{Combinações de Indutor (L), Capacitor (C), Resistor (R) e Circuito Aberto (0) Ensaiadas.}
  \label{quad:combinacoes}
  \centering
  \begin{tabular}[p]{| c | c | c |}
    \hline
    \textbf{Fase A} & \textbf{Fase B} & \textbf{Fase C} \\\hline
    0      &   L    &   0    \\\hline
    0      &  L+R   &   0    \\\hline
    0      &   C    &   0    \\\hline
    0      &  C+R   &   0    \\\hline
    0      &   L    &   L    \\\hline
    0      &   C    &   C    \\\hline
    0      &   L    &   C    \\\hline
    0      &   C    &   L    \\\hline
    L      &   L    &   C    \\\hline
    L      &   C    &   C    \\\hline
    L      &   L    &   L    \\\hline
    C      &   C    &   C    \\\hline
    R      &   R    &   R    \\\hline
    R      &   C    &   L    \\\hline
  \end{tabular}
  \source
\end{quadro}

Também foram ensaiadas as irregularidades de inversão de condutores de corrente e de inversão da sequência de fases das tensões em relação à sequência de fases das correntes. Para os ensaios das irregularidades as conexões do medidor foram manipuladas em seus bornes à semelhança dos casos encontrados em campo.

% A figura \ref{fig:ci_ensaio} apresenta as conexões do medidor - regulares e irregulares - em um ensaio de inversão de condutores de corrente. Na figura \ref{fig:ci_ensaio} a) é perceptível que o condutor vermelho - que vem da rede de alimentação - encontra-se conectado no borne de corrente superior da fase C, enquanto o condutor azul - que vai à carga - encontra-se conectado no borne inferior. Essa configuração de condutores encontra-se invertida na figura \ref{fig:ci_ensaio} b).

% \begin{figure}[htpb]

%   \caption{Conexões do Medidor regulares (a) e irregulares (b) em Ensaio de Inversão de Condutores de Corrente}

%   \begin{multicols}{2}
%     \begin{center}
%       \includegraphics[scale=0.17]{./Imagens/ci_ensaio_ok.jpeg}

%       a)
%     \end{center}

%     \columnbreak

%     \begin{center}
%       \includegraphics[scale=0.20]{./Imagens/ci_ensaio_nok.jpeg}

%       b)
%     \end{center}
%   \end{multicols}
%   \source
%   \label{fig:ci_ensaio}
% \end{figure}

% A figura \ref{fig:si_ensaio} apresenta as conexões do medidor - regulares e irregulares - em um ensaio de inversão da sequência de fases. Na figura \ref{fig:si_ensaio} a) é perceptível que o condutor vermelho - que serve à amostragem de tensão da fase C - encontra-se conectado ao condutor de alimentação da fase C, enquanto o condutor azul - que serve à amostragem de tensão da fase B - encontra-se conectado ao condutor de alimentação da fase B. Essa configuração de condutores encontra-se invertida na figura \ref{fig:si_ensaio} b).

% \begin{figure}[htpb]
%   \caption{Conexões do Medidor regulares (a) e irregulares (b) em Ensaio de Inversão de Sequência de Fases.}
%   \begin{multicols}{2}
%     \begin{center}
%       \includegraphics[height=5cm]{./Imagens/si_ensaio_ok.jpeg}

%       a)
%     \end{center}

%     \columnbreak

%     \begin{center}
%       \includegraphics[height=5cm]{./Imagens/si_ensaio_nok.jpeg}

%       b)
%     \end{center}
%   \end{multicols}
%   \source
%   \label{fig:si_ensaio}
% \end{figure}